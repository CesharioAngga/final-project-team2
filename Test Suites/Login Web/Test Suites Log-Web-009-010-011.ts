<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suites Log-Web-009-010-011</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>161ba2b7-cf26-4fd9-80b7-876c2d55d9f7</testSuiteGuid>
   <testCaseLink>
      <guid>502916ac-eb33-46b3-8d74-78a2b3b9a0c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Web/Log-Web-009-010-011</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>feca5716-d2b7-4f31-a761-970985485d38</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DDT Log-Web-009-010-011</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>feca5716-d2b7-4f31-a761-970985485d38</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>212a268b-b693-4ee2-a841-6eb4782dfafc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>feca5716-d2b7-4f31-a761-970985485d38</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>ddd672d6-4d37-4d01-9ef6-f98619429e2c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
