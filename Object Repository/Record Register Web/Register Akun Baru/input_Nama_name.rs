<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Nama_name</name>
   <tag></tag>
   <elementGuidId>6455ef63-2a0c-4c60-9cda-05f55bb4e0c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e6fd46e5-a246-4879-9cf0-ef7a06ac5ff9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>97a80e5e-a12b-41f3-84a6-9ac9ed43d27e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Nama Anda</value>
      <webElementGuid>d675bdc7-ccf7-4a1c-8840-3d5734de745c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>358d207b-fb67-4d85-8e17-9ebcddc37c05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>2db4668c-3a77-482e-8edd-bd1abc952550</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>39987b57-14bf-4348-aeea-1770b28076bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>name</value>
      <webElementGuid>9b9bd073-8767-4de3-b768-3c9a817ffdc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;name&quot;)</value>
      <webElementGuid>80a8b6c1-638c-4799-aa79-61a313835ff3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='name']</value>
      <webElementGuid>73dd760f-d0b4-4868-bda9-a51230acfece</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input</value>
      <webElementGuid>a6c77562-1016-4c2c-98a9-6b00916443b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'name' and @placeholder = 'Nama Anda' and @type = 'text' and @name = 'name']</value>
      <webElementGuid>2249c1d1-565a-4611-9d38-96c415290698</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
