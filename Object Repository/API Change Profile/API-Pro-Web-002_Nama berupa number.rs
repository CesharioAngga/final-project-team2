<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>API-Pro-Web-002_Nama berupa number</name>
   <tag></tag>
   <elementGuidId>846148ce-2908-4f13-ae77-4c4519ea89c5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTMxYTA3OTc0ZWI0YTNhZDVmYmY1YWE2MzY5NzkxZWFlZWFiNTlhZWFkNGU0NDFlY2Y4ODUyNDFhNjNmN2U4ZTMyNTNmMWY4YzcwOWVkNDkiLCJpYXQiOjE2OTgxNjIzNDQuMTM2MzgxLCJuYmYiOjE2OTgxNjIzNDQuMTM2Mzg0LCJleHAiOjE3Mjk3ODQ3NDQuMTMzNTA1LCJzdWIiOiIxMjA4Iiwic2NvcGVzIjpbXX0.Ko5SzEbb5ps1nOcLbZldQ6YTAhsEDS1AENzUwry2jax_XSeMqbb9IC5BqSVb5aXLPvGhaHCpKAJ_FqfHFXpVO-gzhmjAz94xqdr8KCeDU_cLaKHTlflAgx3vCKksA4SiaANXW0krO31uKkpKTgXXnRJfLrCGG-7RH-KOH3piB8K_9XGJGZ0Faz2VztjrTqSjqXOpljvhtqmEeAUwhfhc7DtT8doU_rttKLeMhF3OgPRW6tDGfd_NaocGnChjnSIHfzKe5x4zGdMQmV66VRASXDN7SWWc3PDrO_pt0qoKWSyOKi8OfQr5SbbXDzRZc5XH2WbYxG-m5CbXmeGr2u7hBC6lltaT9W0ukDCTPuAXKVyjDlmdQygaviCVRyANelfS-T_hV2Nv8ClEja2EXbyuYHLrSXwHcm3WcczPocFmez3Gpfli8VSkqNTd-6Zi_f9puouEL_EliDh1bzenB7OdHOJu4nEUYboXZcp5vmKJ5-YVzb7Qn3dyL7M0SECeK5lLGj702nZITKeBoxJY2vBY_Zchitm6AKEwWH8Xx88rOCrG6WPs1nk8KaAvr21UORws255QpvQh52uT7Lbe3iJcQhUUGD-xuOWkXvhH6k9hIl6JdnHauCaGnPZoouxyXb5fH65mKv44BQOGrUNEEu7-MzyuL32Ym3iLqg5Id41K1gg</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;12345&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;082285974099&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;2000-10-01&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;C:\\Users\\wlan.DESKTOP-33IVI7L\\OneDrive\\Documents\\WEB\\Register web\\Reg-Web-001 (1).JPG&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;Software Dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;mobile dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>9d874db2-2d09-4cdf-ab19-9f1d6a2c7380</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTMxYTA3OTc0ZWI0YTNhZDVmYmY1YWE2MzY5NzkxZWFlZWFiNTlhZWFkNGU0NDFlY2Y4ODUyNDFhNjNmN2U4ZTMyNTNmMWY4YzcwOWVkNDkiLCJpYXQiOjE2OTgxNjIzNDQuMTM2MzgxLCJuYmYiOjE2OTgxNjIzNDQuMTM2Mzg0LCJleHAiOjE3Mjk3ODQ3NDQuMTMzNTA1LCJzdWIiOiIxMjA4Iiwic2NvcGVzIjpbXX0.Ko5SzEbb5ps1nOcLbZldQ6YTAhsEDS1AENzUwry2jax_XSeMqbb9IC5BqSVb5aXLPvGhaHCpKAJ_FqfHFXpVO-gzhmjAz94xqdr8KCeDU_cLaKHTlflAgx3vCKksA4SiaANXW0krO31uKkpKTgXXnRJfLrCGG-7RH-KOH3piB8K_9XGJGZ0Faz2VztjrTqSjqXOpljvhtqmEeAUwhfhc7DtT8doU_rttKLeMhF3OgPRW6tDGfd_NaocGnChjnSIHfzKe5x4zGdMQmV66VRASXDN7SWWc3PDrO_pt0qoKWSyOKi8OfQr5SbbXDzRZc5XH2WbYxG-m5CbXmeGr2u7hBC6lltaT9W0ukDCTPuAXKVyjDlmdQygaviCVRyANelfS-T_hV2Nv8ClEja2EXbyuYHLrSXwHcm3WcczPocFmez3Gpfli8VSkqNTd-6Zi_f9puouEL_EliDh1bzenB7OdHOJu4nEUYboXZcp5vmKJ5-YVzb7Qn3dyL7M0SECeK5lLGj702nZITKeBoxJY2vBY_Zchitm6AKEwWH8Xx88rOCrG6WPs1nk8KaAvr21UORws255QpvQh52uT7Lbe3iJcQhUUGD-xuOWkXvhH6k9hIl6JdnHauCaGnPZoouxyXb5fH65mKv44BQOGrUNEEu7-MzyuL32Ym3iLqg5Id41K1gg</value>
      <webElementGuid>5cfaf7f1-b5c2-45f4-b445-c039132fb807</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.site/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
