<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Verifiy Upload Error Page</name>
   <tag></tag>
   <elementGuidId>576abb4f-f7ab-486d-81cc-2fb74daf466b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ui-exception-message.ui-exception-message-full</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d4df642d-8a63-4ba7-8a65-a73a80e166b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-exception-message ui-exception-message-full</value>
      <webElementGuid>f41cfebe-4df1-40e4-af86-26abf26f9e52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
</value>
      <webElementGuid>0baea77d-0dd4-4ba7-bb0f-fe8b6bb20b42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;theme-light&quot;]/body[@class=&quot;scrollbar-lg&quot;]/div[1]/div[1]/div[@class=&quot;layout-col z-10&quot;]/div[@class=&quot;mt-12 card card-has-header card-no-props&quot;]/div[@class=&quot;card-details&quot;]/div[@class=&quot;card-details-overflow scrollbar p-12 pt-10&quot;]/div[@class=&quot;text-2xl&quot;]/div[@class=&quot;ui-exception-message ui-exception-message-full&quot;]</value>
      <webElementGuid>62f67c67-047e-4066-b8d7-745c2e63a567</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NotReadableException'])[1]/following::div[1]</value>
      <webElementGuid>bfd8b9d7-1882-464f-9877-e3a09e40a8e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exception\'])[1]/following::div[1]</value>
      <webElementGuid>68788c42-0dd5-4496-af8c-524ab0e00076</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='https://demo-app.site/dashboard/profile/update'])[1]/preceding::div[1]</value>
      <webElementGuid>7a21a397-a8a6-4e6e-8021-4abe1a19b621</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stack trace'])[1]/preceding::div[2]</value>
      <webElementGuid>2ebcfb52-11e3-41e5-a952-89cdc0ce499c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.']/parent::*</value>
      <webElementGuid>57e0aad1-2049-4d8a-aecb-fa0c91dd308f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>6453f439-9d38-43c0-b866-c413537a5d25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
    Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
' or . = '
    Unsupported image type application/pdf. GD driver is only able to decode JPG, PNG, GIF, BMP or WebP files.
')]</value>
      <webElementGuid>bd47ac46-0d1a-42bb-8c20-f5394cd2588d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
