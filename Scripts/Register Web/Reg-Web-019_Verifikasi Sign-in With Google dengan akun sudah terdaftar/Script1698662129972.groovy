import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.site/daftar?')

WebUI.click(findTestObject('Record Register Web/Verify Sign-in With Google/Page_Buat akun dan dapatkan akses di Coding.ID/button_Sign-in With Google'))

WebUI.setText(findTestObject('Record Register Web/Verify Sign-in With Google/Page_Sign in - Google Accounts/input_demo-app.online_identifier'), 
    'jes97.yo@gmail.com')

WebUI.click(findTestObject('Record Register Web/Verify Sign-in With Google/Page_Sign in - Google Accounts/span_Next'))

WebUI.setEncryptedText(findTestObject('Record Register Web/Verify Sign-in With Google/Page_Sign in - Google Accounts/input_Too many failed attempts_Passwd'), 
    'p4y+y39Ir5NdXhWXHVnXCA==')

WebUI.click(findTestObject('Record Register Web/Verify Sign-in With Google/Page_Sign in - Google Accounts/span_Next(2)'))

WebUI.verifyElementVisible(findTestObject('Record Register Web/Verify Sign-in With Google/verify 2/Page_Be a Profressional Talent with Coding.ID/halaman beranda Coding ID'))

WebUI.closeBrowser()

