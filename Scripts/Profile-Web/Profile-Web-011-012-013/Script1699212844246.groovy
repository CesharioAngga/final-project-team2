import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.site/login')

WebUI.setText(findTestObject('Change Foto Profile Web/login/input_Email_email'), 'tumbalakun238@gmail.com')

WebUI.setEncryptedText(findTestObject('Change Foto Profile Web/login/input_Kata                                 _98da12'), 
    'j3Y/YglrbE10x3ogLtBSkw==')

WebUI.click(findTestObject('Change Foto Profile Web/login/button_Login'))

WebUI.click(findTestObject('Change Foto Profile Web/klik icon profile di beranda'))

WebUI.click(findTestObject('Change Foto Profile Web/menuju ke halaman profile'))

WebUI.click(findTestObject('Change Foto Profile Web/Profile Page/memilih profile page'))

WebUI.click(findTestObject('Change Password/menuju ganti password'))
for (def rowNum = 1; rowNum <= findTestData('TDV-Change Password').getRowNumbers(); rowNum++) {

WebUI.setText(findTestObject('Change Password/input_Old Password'), findTestData('TDV-Change Password').getValue(1, rowNum))

WebUI.setText(findTestObject('Change Password/input_New Password'), findTestData('TDV-Change Password').getValue(2, rowNum))

WebUI.setText(findTestObject('Change Password/input_Confirmation'), findTestData('TDV-Change Password').getValue(3, rowNum))

WebUI.click(findTestObject('Change Password/button_Save Changes'))

WebUI.verifyElementVisible(findTestObject('Change Password/verify change password gagal'))
}

WebUI.closeBrowser()

