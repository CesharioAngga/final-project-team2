import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\lain-lain\\ces\\QUALITY ASSURANCE\\CODING.ID (QUALITY ASSURANCE)\\Advance Class\\DemoAppV2.apk', 
    true)

Mobile.tap(findTestObject('Object Repository/Record Register Mobile/Button go to Login'), 0)

Mobile.tap(findTestObject('Object Repository/Record Register Mobile/Menuju halaman register'), 0)

Mobile.setText(findTestObject('Object Repository/Record Register Mobile/memasukkan nama'), 'jessica', 0)

Mobile.tap(findTestObject('Object Repository/Record Register Mobile/klik kalender'), 0)

Mobile.tap(findTestObject('Object Repository/Record Register Mobile/selesai memilih tanggal register'), 0)

Mobile.setText(findTestObject('Object Repository/Record Register Mobile/memasukkan email'), 'liyojoj607@ustorp.com', 0)

Mobile.setText(findTestObject('Object Repository/Record Register Mobile/memasukkan nomor'), '082285974099', 0)

Mobile.setEncryptedText(findTestObject('Object Repository/Record Register Mobile/memasukkan password'), 'iGDxf8hSRT4=', 
    0)

Mobile.setEncryptedText(findTestObject('Object Repository/Record Register Mobile/konfirmasi password'), 'iGDxf8hSRT4=', 
    0)

Mobile.tap(findTestObject('Object Repository/Record Register Mobile/klik check box'), 0)

Mobile.tap(findTestObject('Object Repository/Record Register Mobile/Button Register'), 0)

WebUI.delay(5)

Mobile.verifyElementVisible(findTestObject('Verify Register Mobile/Virify Lets join our community'), 0)

Mobile.closeApplication()

